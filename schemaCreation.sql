SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Call_Type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Call_Type` (
  `call_type` VARCHAR(45) NOT NULL ,
  `call_type_number` INT NOT NULL ,
  PRIMARY KEY (`call_type_number`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Call_Entity`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Call_Entity` (
  `call_number` INT NOT NULL ,
  `call_date` DATE NULL ,
  `call_type_number` INT NULL ,
  PRIMARY KEY (`call_number`) ,
  INDEX `fk_call_type_number_idx` (`call_type_number` ASC) ,
  CONSTRAINT `fk_call_type_number`
    FOREIGN KEY (`call_type_number` )
    REFERENCES `mydb`.`Call_Type` (`call_type_number` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Unit_Type`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Unit_Type` (
  `unit_type_number` INT NOT NULL ,
  `unit_type` VARCHAR(24) NOT NULL ,
  PRIMARY KEY (`unit_type_number`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Unit`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Unit` (
  `unit_id` VARCHAR(255) NOT NULL ,
  `unit_type_number` INT NOT NULL ,
  PRIMARY KEY (`unit_id`) ,
  INDEX `unit_type_number_idx` (`unit_type_number` ASC) ,
  CONSTRAINT `unit_type_number`
    FOREIGN KEY (`unit_type_number` )
    REFERENCES `mydb`.`Unit_Type` (`unit_type_number` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Incident`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Incident` (
  `incident_number` INT NOT NULL ,
  `call_number` INT NOT NULL ,
  `adress` VARCHAR(255) NOT NULL ,
  `city` VARCHAR(255) NOT NULL ,
  `zipcode_of_incident` VARCHAR(255) NULL ,
  `box` VARCHAR(255) NULL ,
  `location_x` FLOAT NULL ,
  `latitude` INT NULL ,
  `als_unit` TINYINT(1) NULL ,
  `call_final_desposition` VARCHAR(255) NULL ,
  `dispatch_timestamp` TIMESTAMP NULL ,
  `transport_timestamp` TIMESTAMP NULL ,
  `entry_timestamp` TIMESTAMP NULL ,
  `received_timestamp` TIMESTAMP NULL ,
  `battalion` VARCHAR(255) NULL ,
  `station_area` VARCHAR(255) NULL ,
  `original_priority` INT NULL ,
  `priority` INT NULL ,
  `final_priority` INT NULL ,
  `location_y` FLOAT NULL ,
  INDEX `call_number_idx` (`call_number` ASC) ,
  PRIMARY KEY (`incident_number`) ,
  CONSTRAINT `call_number`
    FOREIGN KEY (`call_number` )
    REFERENCES `mydb`.`Call_Entity` (`call_number` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Unit2Incedent`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `mydb`.`Unit2Incedent` (
  `unit_id` VARCHAR(255) NOT NULL ,
  `incident_number` INT NOT NULL ,
  `response_timestamp` TIMESTAMP NULL ,
  `on_scene_timestamp` TIMESTAMP NULL ,
  INDEX `fk_Unit2Incedent_Unit1_idx` (`unit_id` ASC) ,
  INDEX `fk_Unit2Incedent_Incident1_idx` (`incident_number` ASC) ,
  PRIMARY KEY (`incident_number`) ,
  CONSTRAINT `fk_Unit2Incedent_Unit1`
    FOREIGN KEY (`unit_id` )
    REFERENCES `mydb`.`Unit` (`unit_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Unit2Incedent_Incident1`
    FOREIGN KEY (`incident_number` )
    REFERENCES `mydb`.`Incident` (`incident_number` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `mydb` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
