import mysql.connector
import pandas
import numpy
import pdb



connection = mysql.connector.connect(user='root', password='17asd8585',
                              host='127.0.0.1',
                              database='mydb')

cursor = connection.cursor()

table_names = ['call_type', 'call_entity', 'unit_type',
                  'unit', 'incident', 'unit2incedent']

base_path = 'C:\\Users\\Kıox\\ceng2008-project\\data\\'
cursor.execute("set @@sql_mode='no_engine_substitution'")

def formInsertSQLClause(table_name,column_name):
    #get_column_names = 'select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME="%s"' % table_name
    #cursor.execute(get_column_names)

    column_names = ""
    for COLUMN_NAME in column_name:
        column_names += COLUMN_NAME + ","

    tk1 = column_names[0:-1]
    values = "%s,"*column_names.count(',')

    tk = values[0:-1]
    clause = "INSERT INTO %s (%s) VALUES (%s)" % (table_name, tk1 , tk)

    print(clause)
    return clause

for table_name in table_names:
    print("rows for %s " % table_name )
    csv_data = pandas.read_csv(base_path + "%s.csv" % table_name)
    column_names = csv_data.columns.to_numpy().tolist()
    InsertSQLClause = formInsertSQLClause(table_name,column_names)
    for entry in range(csv_data.shape[0]):
        data = tuple(csv_data.take([entry]).to_numpy().tolist()[0])
        for i in range(len(data)):
            if isinstance(data[i], numpy.generic):
                data[i] = numpy.item(data[i])
        try:
            tempList = list(data)
            new_items = ['NULL' if type(x) == float else x for x in tempList]
            data = new_items
            if table_name == 'incident':

                InsertSQLClause = "INSERT INTO incident (incident_number,call_number,adress,city,zipcode_of_incident,box,latitude,als_unit,call_final_desposition,dispatch_timestamp,transport_timestamp,entry_timestamp,received_timestamp,battalion,station_area,original_priority,priority,final_priority) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
                tempList = list(data)
                permList = tempList.pop(6)
                data = tuple(tempList)

            """if table_name == 'unit2incedent':
                tempList = list(data)
                new_items = ['NULL' if type(x) == float else x for x in tempList]
                data = new_items"""
            cursor.execute(InsertSQLClause, data)
        except Exception as e:
            #template = "An exception of type {0} occurred. Arguments:\n{1!r}"
            #message = template.format(type(e).__name__, e.args)
            #print(message + table_name)
            pass

updateXLocationField = "UPDATE incident SET location_x = %s"
updateYLocationField = "UPDATE incident SET location_y = %s"
csv_incident_data = pandas.read_csv(base_path + "%s.csv" % 'incident')
for i in range(csv_incident_data.shape[0]):
    cursor.execute("UPDATE incident SET location_x = %s WHERE incident_number = %s" % (float(csv_incident_data['location'].to_numpy()[i][2:-1].split(',')[0]),int(csv_incident_data['incident_number'][i])))
    cursor.execute("UPDATE incident SET location_y = %s WHERE incident_number = %s" % (float(csv_incident_data['location'].to_numpy()[i][2:-1].split(',')[0]),int(csv_incident_data['incident_number'][i])))
    #cursor.execute(updateXLocationField,(float(csv_incident_data['location'].to_numpy()[i][2:-1].split(',')[0])))
    #cursor.execute(updateYLocationField,(float(csv_incident_data['location'].to_numpy()[i][2:-1].split(',')[1])))
connection.commit()

